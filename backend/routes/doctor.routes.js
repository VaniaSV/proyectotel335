const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Doctor = require('../models/doctor');


//Obtener lista de doctores
router.get('/' , (req,res)=> 
Doctor.findAll()
    .then(doctores => {
        res.send(doctores)
    })
    .catch(err=>console.log(err))
);
//Obtener doctor específico
router.get("/:id", async(req,resp)=>{
    try{
        const allDoc = await Doctor.findAll({where:req.params});
        resp.send(allDoc);
    }catch(error){
        resp.status(400).send("Error");
    }
});
//Iniciar Sesión
router.post("/login", async (req, res) => {
    try {
      const user = await Doctor.findOne({
        where: {
          id: req.body.rut,
        },
      });
      if (!user) return res.status(400).send("usuario o contrasena equivocada");
      if(req.body.contrasena == user.contrasena){
          return res.status(200).send("Ha sido loggeado de forma exitosa")
      }
      else {
          return res.status(400).send("Contraseña incorrecta")
      }
    } catch (error) {
      return res.status(400).send(error);
    }
  });



module.exports = router;