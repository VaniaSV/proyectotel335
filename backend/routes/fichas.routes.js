const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Fichas = require('../models/fichas');


//Obtener lista de fichas
router.get('/' , (req,res)=> 
Fichas.findAll()
    .then(fichas => {
        res.send(fichas)
    })
    .catch(err=>console.log(err))
);
//Obtener ficha específica
router.get("/:id", async(req,resp)=>{
    try{
        const allFic = await Fichas.findAll({where:req.params});
        resp.send(allFic);
    }catch(error){
        resp.status(400).send("Error");
    }
});
//Si no tiene ficha
router.post("/register",async(req,resp)=>{
    try{
        const userValid = await Fichas.findOne({
            where: {
              id: req.body.rut,
              numero_documento: req.body.numero_documento,
            },
          });
          //se crea el usuario
          const user = await Fichas.update({
            ficha: req.body.ficha,
          },
          {where:
            {
                id:req.params.rut
            }
          });
          return resp.send(user);
          
    }catch(error){
        resp.status(400).send(error);
    }
});
module.exports = router;