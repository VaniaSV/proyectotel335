const Sequelize = require('sequelize');
const db = require('../config/database');

const Doctor = db.define('fichas', {
    ficha:{
        type: Sequelize.TEXT
    },
    numero_documento:{
        type: Sequelize.INTEGER
    }
})

module.exports = Doctor