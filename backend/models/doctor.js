const Sequelize = require('sequelize');
const db = require('../config/database');

const Doctor = db.define('doctor', {

    nombre:{
        type: Sequelize.STRING
    },
    apellido:{
        type: Sequelize.STRING
    },
    especialidad:{
        type: Sequelize.STRING
    },
    profesion:{
        type: Sequelize.STRING
    },
    contrasena:{
        type: Sequelize.STRING
    }
})

module.exports = Doctor